import 'package:get_it/get_it.dart';
import 'package:poc_custom_theme/injection_sample/app_config.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerLazySingleton<AppConfig>(() => AppConfig());
}


import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:poc_custom_theme/injection_sample/app_config.dart';
import 'package:poc_custom_theme/injection_sample/injection_container.dart';

import 'app_config.dart';

class InjectionSampleApp extends StatelessWidget {
  const InjectionSampleApp({super.key});

  @override
  Widget build(BuildContext context) {
    final AppConfig config = sl<AppConfig>();

    return MaterialApp(
      title: 'POC Custom Theme',
      debugShowCheckedModeBanner: false,
      themeMode: config.themeMode,
      theme: lightTheme,
      darkTheme: darkTheme,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  AppConfig config = sl<AppConfig>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: config.theme.primaryColor,
        title: Text(
          'POC Custom Theme',
          style: config.theme.textTheme.headlineMedium,
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Press the button to manually change theme',
              style: config.theme.textTheme.bodyMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: config.theme.primaryColor,
        onPressed: () {
          config.toggleTheme();
          setState(() {
          });
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}




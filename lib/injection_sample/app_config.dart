import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class AppConfig {
  ThemeMode themeMode = ThemeMode.system;

  void toggleTheme() {
    if((themeMode == ThemeMode.system)){   //first time handle for device default theme
      if (isSystemDarkMode()) {
        themeMode = ThemeMode.light;
      } else {
        themeMode = ThemeMode.dark;
      }
    }else{                                  ////handle app toggle
      if(themeMode == ThemeMode.dark){
        themeMode = ThemeMode.light;
      }else{
        themeMode = ThemeMode.dark;
      }
    }
  }

  bool isSystemDarkMode() {
    Brightness brightness = SchedulerBinding.instance.window.platformBrightness;
    return brightness == Brightness.dark;
  }

  ThemeData get theme {
    switch (themeMode) {
      case ThemeMode.system:
        if (isSystemDarkMode())
          return darkTheme;
        else
          return lightTheme;
      case ThemeMode.dark:
        return darkTheme;
      case ThemeMode.light:
        return lightTheme;
    }
  }
}

ThemeData get lightTheme => ThemeData(
      primaryColor: Colors.blue,
      secondaryHeaderColor: Colors.blue,
      textTheme: const TextTheme(
        headlineMedium: TextStyle(
          color: Colors.black,
          fontSize: 20,
        ),
        bodyMedium: TextStyle(
          color: Colors.black,
          fontSize: 16,
        ),
      ),
    );

ThemeData get darkTheme => ThemeData(
      primaryColor: Colors.black,
      secondaryHeaderColor: Colors.black,
      textTheme: const TextTheme(
        headlineMedium: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
        bodyMedium: TextStyle(
          color: Colors.black,
          fontSize: 16,
        ),
      ),
    );

// abstract class CustomThemeData {
//   Color get primaryColor;
//   Color get backgroundColor;
//   TextTheme? get textColor;
// }
//
// class LightThemeData implements CustomThemeData {
//   Color get primaryColor => Colors.blue;
//   Color get backgroundColor => Colors.blue;
//   TextTheme? get textColor => const TextTheme(
//     headlineMedium: TextStyle(
//       color: Colors.black,
//       fontSize: 24,
//     ),
//   );
// }
//
// class DarkThemeData implements CustomThemeData {
//   Color get primaryColor => Colors.black;
//   Color get backgroundColor => Colors.black;
//   TextTheme? get textColor => const TextTheme(
//     headlineMedium: TextStyle(
//       fontSize: 24,
//       color: Colors.white,
//     ),
//   );
// }

import 'package:flutter/material.dart';
import 'package:poc_custom_theme/injection_sample/injection_sample_app.dart';
import 'package:poc_custom_theme/injection_sample/injection_container.dart' as di;

void main() {
  di.init();
  runApp(const InjectionSampleApp());
}


